<?php

namespace App\Classes\Socket;

use App\Classes\Socket\Base\BaseSocket;
use App\Message;
use App\User;
use Illuminate\Support\Facades\Session;
use Ratchet\ConnectionInterface;

class ChatSocket extends BaseSocket
{
    protected $clients;

    public $colors = [
        ['red', 'black'],
        ['blue', 'red'],
        ['black', 'yellow'],
        ['yellow', 'red'],
        ['black', 'blue']
    ];

    public $color;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage();
        $this->color = $this->setColor();
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $msg = json_decode($msg);

        if (!empty($msg->message)) {
            $lastUserMessage = Message::orderBy('id', 'DESC')->where('user_id', $msg->user_id)->first();

            if (!empty($lastUserMessage)) {

                $secDiff = time() - strtotime($lastUserMessage->created_at);

                if ($secDiff > 15) {
                    $this->returnMessage($msg);
                } else {
                    $msg->timeout = true;
                }

            } else {
                $this->returnMessage($msg);
            }
        }

        if (!empty($msg->ban)) {
            $this->banUser($msg);
        }

        foreach ($this->clients as $client) {
            $client->send(json_encode($msg));
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occured: {$e->getMessage()}\n";

        $conn->close();
    }

    public function banUser(&$msg)
    {
        $user = User::where('id', $msg->ban)->first();
        $user->is_enabled = false;
        $user->save();
        return $msg->ban = true;
    }

    public function createMessage(&$msg)
    {
        $message = new Message();
        $message->message = $msg->message;
        $message->user_id = $msg->user_id;
        $message->save();

        return $message->id;
    }

    public function returnMessage(&$msg)
    {
        $msgId = $this->createMessage($msg);

        $user = User::where(['is_enabled' => true, 'id' => $msg->user_id])->first();
        $msg = Message::where('id', $msgId)->first();
        $msg->user = $user;
        $msg->color = $this->color;
    }

    public function setColor()
    {
        return $this->colors[mt_rand(0, count($this->colors) - 1)];
    }
}