<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {

        $initMessages = Message::orderBy('created_at', 'DESC')->take(5)->get();

        $users = User::all();

        foreach ($users as $user) {
            foreach ($initMessages as $message) {
                if ($user->id == $message->user_id) {
                    $message->user = $user;
                }
            }
        }

        return view('home', ['messages' => $initMessages->reverse()]);
    }
}
