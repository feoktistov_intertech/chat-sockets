@extends('layouts.app')

@section('content')
    <div class="container">

        <?php if (Auth::user()->is_enabled): ?>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">MESSENGER</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div id="flash"></div>
                        <div id="timeline">

                            <?php if (!empty($messages)): ?>
                            <?php foreach ($messages as $k => $message): ?>

                            <div class="tweet my-tweet">
                                <div class="content">
                                    <div class="stream-item-header">
                                        <a class="account-group">
                                            <span><strong class="fullname show-popup-with-id">
                                                    <span>{{ $message->user->username }}</span></strong>
                                                    <span></span>
                                                    <span>&nbsp;</span>
                                            </span>
                                        </a>
                                        <small class="time">
                                            <a><span>{{ $message->created_at }}</span></a>
                                            <?php if (Auth::user()->is_admin && Auth::user()->id !== $message->user_id): ?>
                                            <a onclick='ban({{ $message->user->id }})'> ban</a>
                                            <?php endif; ?>
                                        </small>
                                    </div>
                                    <div>
                                        <p class="tweet-text"><a class="twitter-atreply pretty-link"></a>
                                            <span>{{ $message->message }}</span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <?php endforeach; ?>
                            <?php endif; ?>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <div id="message">
                                    <textarea style="width: 470px"></textarea>
                                </div>
                            </div>
                        <div class="col-xs-1"></div>
                    </div>

                    <div class="row">
                        <div class="col-xs-4"></div>
                            <div class="col-xs-4" style="margin-left: 50px; margin-bottom: 10px">
                                <a class="button btn btn-danger" onclick="send();">Send</a>
                            </div>
                        <div class="col-xs-2"></div>
                    </div>

                </div>
            </div>
        </div>

        <script>
            var is_admin = {{ Auth::user()->is_admin }};
            var user_id = {{ Auth::user()->id }};

            var conn = new WebSocket('ws://localhost:8080');

            conn.onopen = function (e) {
                console.log('Connected!');
            };

            conn.onmessage = function (e) {
                var message = JSON.parse(e.data);

                var flash = $('#flash');
                flash.html('');

                if (message.ban) {
                    window.location = '/home';
                }

                if (message.timeout) {
                    flash.html('<span class="alert-success">You are sending messages too often</span>'); return;
                }

                var color = message.color;

                $('#timeline').append(
                    '<div class="tweet my-tweet"><div class="content"><div class="stream-item-header"><a class="account-group"><span><strong class="fullname show-popup-with-id" ><span style="color:' + color[0] + '">' + message.user.username + '</strong><span></span><span>&nbsp;</span></span></a><small class="time"><a><span time>' + message.created_at + '</span></a>' + (is_admin && user_id !== message.user_id ? ' <a onclick="ban(' + message.user.id + ')">ban</a>' : '') + '</small><div><p class="tweet-text"><a class="twitter-atreply pretty-link"></a><span style="color:' + color[1] + '">' + message.message + '</p></div></div></div></div>'
                );
            };

            function send() {
                var message = $('#message textarea').val();
                if (message.length === 0) {
                    return;
                }
                var data = {message: message, user_id: user_id};
                conn.send(JSON.stringify(data));
                $('#message textarea').val('');
            }

            function ban(user_id) {
                var data = {ban: user_id};
                conn.send(JSON.stringify(data));
            }
        </script>

        <?php else: ?>

        <h1>You has beed excepted from the messenger!</h1>

        <?php endif; ?>
    </div>
@endsection
